<?php


namespace App\Models;


class ShopProduct
{
    private $id;
    private $title;
    private $mainName;
    private $firstName;
    protected $price;
    protected $type;

    public function __construct(
        string $id,
        string $title,
        string $firstName,
        string $mainName,
        float $price,
        string $type
    )
    {
        $this->id = $id;
        $this->title = $title;
        $this->firstName = $firstName;
        $this->mainName = $mainName;
        $this->price = $price;
        $this->type = $type;
    }

    public
    function getId(): string
    {
        return $this->id;
    }

    public
    function getFirstName(): string
    {
        return $this->firstName;
    }

    public
    function getMainName(): string
    {
        return $this->mainName;
    }

    public
    function getTitle(): string
    {
        return $this->title;
    }

    public
    function getPrice(): float
    {
        return ($this->price);
    }

    public
    function getFullName(): string
    {
        return $this->firstName . " " . $this->mainName;
    }

    public function getType(): string
    {
        return $this->type;
    }
}
