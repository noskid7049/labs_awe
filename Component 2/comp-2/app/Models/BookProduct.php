<?php


namespace App\Models;


class BookProduct extends ShopProduct
{
    private $numPages;

    public function __construct(
        string $id,
        string $title,
        string $firstName,
        string $mainName,
        float $price,
        int $numPages,
        string $type
    )
    {

        parent::__construct(
            $id,
            $title,
            $firstName,
            $mainName,
            $price,
            $type
        );
        $this->numPages = $numPages;
    }

    public function getNumberOfPages(): int
    {
        return $this->numPages;
    }
}
