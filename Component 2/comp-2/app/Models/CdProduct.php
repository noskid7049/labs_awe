<?php


namespace App\Models;


class CdProduct extends ShopProduct
{
    private $playLength;

    public function __construct(
        string $id,
        string $title,
        string $firstName,
        string $mainName,
        float $price,
        int $playLength,
        string $type
    )
    {
        parent::__construct(
            $id,
            $title,
            $firstName,
            $mainName,
            $price,
            $type
        );
        $this->playLength = $playLength;
    }

    public function getPlayLength(): int
    {
        return $this->playLength;
    }
}
