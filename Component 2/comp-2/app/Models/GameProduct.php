<?php


namespace App\Models;


class GameProduct extends ShopProduct
{
    private $pegi;

    public function __construct(
        string $id,
        string $title,
        string $firstName,
        string $mainName,
        float $price,
        int $pegi,
        string $type
    )
    {
        parent::__construct(
            $id,
            $title,
            $firstName,
            $mainName,
            $price,
            $type
        );
        $this->pegi = $pegi;
    }
    public function getPegi(): int
    {
        return $this->pegi;
    }
}
