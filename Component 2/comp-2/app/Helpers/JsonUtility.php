<?php


namespace App\Helpers;

use App\Models\BookProduct;
use App\Models\CdProduct;
use App\Models\GameProduct;

use phpDocumentor\Reflection\Types\This;

class JsonUtility
{
    public static function makeProductArray(string $file)
    {
        $string = file_get_contents($file);

        return self::jsonToArray($string);
    }

    public static function deleteProductWithId(string $file, int $id)
    {
//      Read the required store and decode the data
        $string = file_get_contents($file);
        $productsJson = json_decode($string, true);
//      Convert the JSON object to array of objects
//      Delete the product object with the given id
        $products = [];
        foreach ($productsJson as $product) {
            if ($product['id'] != $id) {
                $products[] = $product;
            }
        }
//      Encode the object array and save to store
        $json = json_encode($products);
        if (file_put_contents($file, $json))
            return true;
        else
            return false;
    }

    public static function getProductWithId(string $file, int $id)
    {
//      Read the required store and decode the data
        $string = file_get_contents($file);

        $productsJson = json_decode($string, true);
//      Convert the JSON object to array and get the object with the given id
        $products = [];
        foreach ($productsJson as $product) {
            if ($product['id'] == $id) {
                $products[] = self::getProduct($product);
                break;
            }
        }
        return $products;
    }

    public static function addNewProduct(string $file, string $producttype, string $title, string $fname, string $sname, float $price, int $extra)
    {
        $string = file_get_contents($file);
//      Decode the JSON file
        $productsJson = json_decode($string, true);
//      Get the list of array
        $ids = [];
        foreach ($productsJson as $product) {
            $ids[] = $product['id'];
        }
//      Reverse sort the array to get the id of the new product
        rsort($ids);
        $newId = $ids[0] + 1;

        $products = [];
        foreach ($productsJson as $product) {
            $products[] = $product;
        }

        $newProduct = [];
        $newProduct['id'] = $newId;
        $newProduct['type'] = $producttype;
        $newProduct['title'] = $title;
        $newProduct['firstname'] = $fname;
        $newProduct['mainname'] = $sname;
        $newProduct['price'] = $price;

        if ($producttype == 'cd') $newProduct['playlength'] = $extra;
        if ($producttype == 'book') $newProduct['numpages'] = $extra;
        if ($producttype == 'game') $newProduct['pegi'] = $extra;

        $products[] = $newProduct;
//      Encode the JSON to put the contents in the file
        $json = json_encode($products);
        if (file_put_contents($file, $json))
            return true;
        else
            return false;
    }

    public static function updateProductWithId(string $file, int $id, string $title, string $fname, string $sname, float $price, int $extra)
    {
//      File Decoding and Object to array conversion
        $string = file_get_contents($file);
        $products = [];
        $productsJson = json_decode($string, true);
//      Update the data of the required object with the given updated data
        foreach ($productsJson as $product) {
            if ($product['id'] == $id) {
                $product['title'] = $title;
                $product['firstname'] = $fname;
                $product['mainname'] = $sname;
                $product['price'] = $price;
                if ($product['type'] == 'cd') $product['playlength'] = $extra;
                if ($product['type'] == 'book') $product['numpages'] = $extra;
                if($product['type'] == 'game') $product['pegi'] = $extra;
            }
            $products[] = $product;
        }
//      Encoding and data store
        $json = json_encode($products);
        if (file_put_contents($file, $json))
            return true;
        else
            return false;
    }

    public static function jsonToArray(string $string)
    {
        $productsJson = json_decode($string, true);

        $products = [];
        foreach ($productsJson as $product) {
            if (self::getProduct($product)) {
                $products[] = self::getProduct($product);
            }
        }
        return $products;
    }
    /*
     * Product Object Generator
     * */
    public static function getProduct($product)
    {
        $reqProduct = null;
        switch ($product['type']) {
            case "cd":
                $reqProduct = new CdProduct($product['id'], $product['title'], $product['firstname'],
                    $product['mainname'], $product['price'], $product['playlength'], $product['type']);
                break;
            case "book":
                $reqProduct = new BookProduct($product['id'], $product['title'], $product['firstname'],
                    $product['mainname'], $product['price'], $product['numpages'], $product['type']);
                break;
            case "game":
                $reqProduct = new GameProduct($product['id'], $product['title'], $product['firstname'],
                    $product['mainname'], $product['price'], $product['pegi'], $product['type']);
                break;
        }
        return $reqProduct;
    }
}
