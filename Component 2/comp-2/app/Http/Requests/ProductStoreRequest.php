<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'fname' => 'required',
            'sname' => 'required',
            'price' => ['required', 'regex:/^(?=.+)(?:[1-9]\d*|0)?(?:\.\d+)?$/'],
            'extra' => ['required','numeric'],
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'A title is required',
            'fname.required' => 'A fname is required',
            'sname.required' => 'A sname is required',
            'sname.required' => 'A fname is required',
            'price.required' => 'A price is required',
            'price.regex' => 'The price format is not matched.',
            'extra.required' => 'A extra is required',
            'extra.numeric' => 'A extra must be of type numeric',
        ];
    }
}
