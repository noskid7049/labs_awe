<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductStoreRequest;
use Illuminate\Http\Request;
use App\Helpers\JsonUtility;

class ProductController extends Controller
{
    /*
     * Location of the product file
    */
    public $PRODUCT_LOC;

    public function __construct()
    {
//      Load the location of the product file either stored in env or the default location
        $this->PRODUCT_LOC = base_path() . config('app.file_path');
    }

    public function index(Request $request)
    {
//      Get the messages if present in session
        $messages = $request->session()->pull('messages');
//      Get the products list
        $productArr = JsonUtility::makeProductArray($this->PRODUCT_LOC);
        return View('home', array('productList' => $productArr, 'messages' => $messages));
    }

    public function show(Request $request, $id)
    {
//      Get the messages if present in session
        $messages = $request->session()->pull('messages');
        $id = intval($id);
//      Get the required product
        $product = JsonUtility::getProductWithId($this->PRODUCT_LOC, $id);
        if (count($product) == 0) abort(404);
        return View('indProduct', array('product' => $product, 'messages' => $messages));
    }

    public function store(ProductStoreRequest $request)
    {
//      Get the product details from the request
        $type = $request->input('producttype');
        $title = $request->input('title');
        $fname = $request->input('fname');
        $sname = $request->input('sname');
        $price = floatval($request->input('price'));
        $extra = intval($request->input('extra'));
//      Add the product
        $added = JsonUtility::addNewProduct($this->PRODUCT_LOC, $type, $title, $fname, $sname, $price, $extra);
        $statusCode = 200;
        $messages = [];
//      Check if the product was added and create session msg array with appropriate data
        if ($added) {
            array_push($messages, 'Product added successfully.');
        }
        $request->session()->put('messages', $messages);
        $json = ['messages' => $messages];
//      Redirect to the /product
        return response()->json($json, $statusCode);
    }

    public function update(ProductStoreRequest $request, $id)
    {
//      Get the product details from the request
        $id = intval($id);
        $title = $request->input('title');
        $fname = $request->input('fname');
        $sname = $request->input('sname');
        $price = floatval($request->input('price'));
        $extra = intval($request->input('extra'));
//      Update and set the appropriate msg in session
        $updated = JsonUtility::updateProductWithId($this->PRODUCT_LOC, $id, $title, $fname, $sname, $price, $extra);
        $statusCode = 200;
        $messages = [];
        if ($updated) {
            array_push($messages, 'Product with ID ' . $id . ' updated successfully.');
        }
        $request->session()->put('messages', $messages);
        $json = ['messages' => $messages];
        return response()->json($json, $statusCode);
    }

    public function destroy(Request $request, $id)
    {
        $id = intval($id);
//      Initially set the product to null and deleted status to false;
        $product = null;
        $deleted = false;
//      Check if the product is available
        $availableProducts = JsonUtility::getProductWithId($this->PRODUCT_LOC, $id);
        if (count($availableProducts) > 0) {
            $product = $availableProducts[0];
        }
//      If product available, delete the product
        if ($product) {
            $deleted = JsonUtility::deleteProductWithId($this->PRODUCT_LOC, $id);
        }
        $statusCode = 200;
        $messages = [];
//      Set the session msg to appropriate data
        if ($deleted) {
            array_push($messages, 'Product with ID ' . $id . ' deleted successfully.');
        }

        $request->session()->put('messages', $messages);
        $json = ['messages' => $messages];
        return response()->json($json, $statusCode);
    }

}
