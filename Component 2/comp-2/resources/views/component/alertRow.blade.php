<div class="row" id="alert-row">
    <div class="col-md-8 mx-auto">
        <div class="alert alert-{{ $type  }} alert-dismissible fade show" role="alert" id="alert">
            {{ $message }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>
