<div class="card product select-product" data-value="{{ $id }}">
    <div class="card-body">
        <div class="card-img-top text-center mb-2">
            <div class="img-container w-50 mx-auto">
                <i class="fa fa-{{ $faClass }} display-4"></i>
            </div>
        </div>
        <h5 class="card-title text-center">{{ $type }}</h5>
        <h6 class="card-subtitle mb-2 text-muted text-center h4">{{ $title }}</h6>
        <div class="card-text">
            <p>{{ $name }}</p>
            <p>{{ $extra }}</p>
            <h1>{{ $price }}</h1>
        </div>
    </div>
</div>
