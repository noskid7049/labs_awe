@component('component/productCard')
    @slot("faClass")
        @switch($product->getType())
            @case("cd")
            compact-disc
            @break
            @case("book")
            book
            @break
            @case("game")
            gamepad
            @break
        @endswitch
    @endslot
    @slot("type")
        {{ strtoupper($product->getType()) }}
    @endslot
    @slot("title")
        {{ $product->getTitle() }}
    @endslot
    @slot("name")
        {{ $product->getFirstName() }} {{ $product->getMainName() }}
    @endslot
    @slot("price")
        €{{ $product->getPrice() }}
    @endslot
    @slot("extra")
        @switch($product->getType())
            @case("cd")
            Play Length : {{ $product->getPlayLength() }}
            @break
            @case("book")
            Number of Pages: {{ $product->getNumberOfPages() }}
            @break
            @case("game")
            PEGI: {{ $product->getPegi() }}
            @break
        @endswitch
    @endslot
    @slot("id")
        {{ $product->getId() }}
    @endslot
@endcomponent
