@component('component/alertRow')
    @slot('type')
        {{ $msg['type'] }}
    @endslot
    @slot('message')
        {{ $msg['message'] }}
    @endslot
@endcomponent
