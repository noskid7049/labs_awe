<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layout.partials.head')
</head>
<body class="py-5">
<div class="container-xl py-5 bg-info">
    @yield('content')
</div>
@include('layout.partials.footer')
@include('layout.partials.footer-scripts')
</body>
</html>
