@if($messages!= null && count($messages)>0)
    <?php
    $reqText = '';
    foreach ($messages as $message) {
        $reqText .= $message;
    }
    ?>
    @include('generator/alertrow',['msg'=>
        ['type'=>'success',
         'message'=>$reqText]
    ])
@endif
