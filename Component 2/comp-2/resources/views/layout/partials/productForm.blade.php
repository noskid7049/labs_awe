<div class="form-group">
    <input type="text" class="form-control" id="title" aria-describedby="Title"
           placeholder="" name="title" required value="{{ isset($product)?$product->getTitle():"" }}">
    <span class="floating-label">Title</span>
    <div class="invalid-tooltip">
    </div>
</div>
<div class="form-group">
    <input type="text" class="form-control" id="fname" aria-describedby="First Name"
           placeholder="" name="fname" required
           value="{{ isset($product)?$product->getFirstName():"" }}">
    <span class="floating-label">First name</span>
    <div class="invalid-tooltip">
    </div>
</div>
<div class="form-group">
    <input type="text" class="form-control" id="sname" aria-describedby="Surname/ Band"
           placeholder="" name="sname" required
           value="{{ isset($product)?$product->getMainName():"" }}">
    <span class="floating-label">Surname/ band</span>
    <div class="invalid-tooltip">
    </div>
</div>
<div class="form-group">
    <input type="text" class="form-control" id="price" aria-describedby="Price"
           placeholder="" name="price" required value="{{ isset($product)?$product->getPrice():"" }}">
    <span class="floating-label">Price</span>
    <div class="invalid-tooltip">
    </div>
</div>
<div class="form-group">
    <input type="text" class="form-control" id="extra"
           aria-describedby="Pages/ Playlength/ PEGI"
           placeholder="" name="extra" required
           value="{{ isset($extra)?$extra:"" }}">
    <span class="floating-label">Pages/ Playlength/ PEGI</span>
    <div class="invalid-tooltip">
    </div>
</div>
