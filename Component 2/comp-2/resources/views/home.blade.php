@extends('layout.master')
@section('content')
    {{--    @if(isset($msg))--}}
    {{--        @include('generator/alertrow',['msg'=>$msg])--}}
    {{--    @endif--}}


    @include('layout.messages')
    <div class="row justify-content-center product-row">
        @foreach($productList as $prod)
            <div class="col-md-4 mb-3">
                @include('generator/card',['product'=>$prod])
            </div>
        @endforeach
    </div>
    <div class="row product-form-row">
        <div class="col-md-8 mx-auto">
            <div class="card">
                <div class="card-body">
                    <div class="card-title text-center">
                        <h2>Add Product</h2>
                    </div>
                    <div class="form-group">
                        <select name="type" id="producttype" class="form-control" required
                                onclick="this.setAttribute('value', this.value);" value="">
                            <option value="" selected disabled></option>
                            <option value="cd">CD</option>
                            <option value="book">Book</option>
                            <option value="game">Game</option>
                        </select>
                        <span class="floating-label">Product Type</span>
                    </div>
                    @include('layout.partials.productForm')
                    <button type="button" class="btn btn-secondary add-product">ADD NEW</button>
                </div>
            </div>

        </div>
    </div>

@endsection
