@extends('layout.master')
@section('content')
    <?php
    $product = $product[0];
    $extra = null;
    switch ($product->getType()) {
        case ("cd"):
            $extra = $product->getPlayLength();
            $faClass = "compact-disc";
            break;
        case("book"):
            $extra = $product->getNumberOfPages();
            $faClass = "book";
            break;
        case('game'):
            $extra = $product->getPegi();
            $faClass = "gamepad";
            break;
    }
    $type = ($product->getType());
    ?>
    @include('layout.messages')
    <div class="row mb-3 product-form-row">
        <div class="col-md-6 mx-auto">
            <div class="card" style="">
                <div class="card-body">
                    <div class="card-img-top text-center mb-2">
                        <div class="img-container w-50 mx-auto">
                            <i class="fa fa-{{ $faClass }} display-4"></i>
                        </div>
                    </div>
                    <h5 class="card-title text-center">
                        {{ strtoupper($type) }}
                    </h5>
                    @include('layout.partials.productForm')

                    <button class="btn btn-secondary float-left update-product" value="{{ $product->getId() }}">UPDATE
                    </button>
                    <button class="btn btn-danger float-right delete-product" value="{{ $product->getId() }}">DELETE
                    </button>
                </div>
            </div>

        </div>
    </div>

@endsection
