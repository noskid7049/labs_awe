window.onload = function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(".select-product").click(async e => {
        var reqValue = e.target.value || e.currentTarget.dataset.value;
        await getProductByID(reqValue);
    })
    $("button.update-product").click(async e => {
        await updateProductByID(e.target.value);
    })
    $("button.delete-product").click(async e => {
        await deleteProductByID(e.target.value);
    })
    $("button.add-product").click(async e => {
        await addNewProduct();
    })
    if ($('#alert')) {
        setTimeout(function () {
            $("#alert").alert('close');
        }, 3000)
    }

    $(document).on('closed.bs.alert', '#alert', function () {
        $("#alert-row").addClass("d-none");
    })
}

function getProductByID(id) {
    window.location = "/product/" + id;
}

async function updateProductByID(id) {
    var data = getFormData();

    //as sending the whole form up - this equates better to a put request

    await axios.put('/product/' + id, data).then((response) => {
        // console.log(response)
        location.reload();
    }).catch(errorParser);
}

async function deleteProductByID(id) {
    await axios.delete('/product/' + id).then(() => {
        console.log("success");
        window.location = "/product"
    }).catch(error => {
        console.error(error)
    });
}

async function addNewProduct() {
    var data = getFormData();

    await axios.post('/product',data).then(() => {
        location.reload();
    }).catch(errorParser);
}

function getFormData() {

    var producttype = document.getElementById('producttype');
    producttype ? producttype = producttype.value : producttype = "";
    var title = document.getElementById('title');
    title ? title = title.value : title = " ";
    var fname = document.getElementById('fname');
    fname ? fname = fname.value : fname = " ";
    var sname = document.getElementById('sname');
    sname ? sname = sname.value : sname = " ";
    var extra = document.getElementById('extra');
    extra ? extra = extra.value : extra = 0;
    var price = document.getElementById('price');
    price ? price = price.value : price = 0;

    return {
        producttype,
        title,
        fname,
        sname,
        extra,
        price
    };
}

function errorParser(error) {
    var errors = error.response.data.errors;
    console.log(errors)
    Object.keys(errors).forEach(key => {
        $(`#${key}`).addClass("is-invalid");
        $(`#${key}~.invalid-tooltip`).text(errors[key]);
    })
    // location.reload();
}

